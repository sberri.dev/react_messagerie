import React from 'react'
import Contact from './components/Contact.jsx'
import './App.css'

function App() {

  return (
    <React.Fragment>
      
      <h1>MSN Messagerie</h1>
     
      <Contact contactName="Josephine Ange-Gardien" contactImage="téléchargement.jpg" onLine={false}/>
      <Contact contactName="Elton John" contactImage="josephine.jpg" onLine={false}/>
      <Contact contactName="Johnny Depp" contactImage="Angelina.jpg" onLine={true}/>
    
    </React.Fragment>
  )
}

export default App
