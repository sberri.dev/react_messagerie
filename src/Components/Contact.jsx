import './Contact.css'
const Contact = function (props) {

    let dotClass = "";

    if (props.onLine)
    dotClass = "online";

    return ( 
    <div className="profile">
        <div className='profile_container'>
            
            <img className="profile-image" src={props.contactImage} />
            <div className={"profile-dot " + dotClass}></div>
           
        </div>
         <h2 className="name">{props.contactName}</h2>
         </div>
    )
}

export default Contact